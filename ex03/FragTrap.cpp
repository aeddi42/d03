/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   FragTrap.cpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aeddi <aeddi@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/08 10:01:34 by aeddi             #+#    #+#             */
/*   Updated: 2015/04/09 16:46:10 by aeddi            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <iostream>
#include <fstream>
#include "FragTrap.hpp"

				FragTrap::FragTrap(void) : ClapTrap() {

	this->_hit_points = 100;
	this->_max_hit_points = 100;
	this->_energy_points = 100;
	this->_max_energy_points = 100;
	this->_melee_attack_damage = 30;
	this->_ranged_attack_damage = 20;
	this->_armor_damage_reduction = 5;

	std::cout << "Directive one: Protect humanity! Directive two: Dance!" << std::endl;
	std::cout << "Commencing directive two! Uhntssuhntssuhntss--" << std::endl;

}

				FragTrap::FragTrap(std::string const & name) : ClapTrap(name) {

	this->_hit_points = 100;
	this->_max_hit_points = 100;
	this->_energy_points = 100;
	this->_max_energy_points = 100;
	this->_melee_attack_damage = 30;
	this->_ranged_attack_damage = 20;
	this->_armor_damage_reduction = 5;

	std::cout << "Directive one: Protect humanity! Directive two: Dance!" << std::endl;
	std::cout << "Commencing directive two! Uhntssuhntssuhntss--" << std::endl;
}


				FragTrap::FragTrap(FragTrap const & src) {

	*this = src;

	std::cout << "Directive one: Protect humanity! Directive two: Dance!" << std::endl;
	std::cout << "Commencing directive two! Uhntssuhntssuhntss--" << std::endl;
}


				FragTrap::~FragTrap(void) {
	std::cout << "I'M DEAD I'M DEAD OHMYGOD I'M DEAD!" << std::endl;
}


FragTrap&		FragTrap::operator=(FragTrap const & src) {

	this->_name = src._name;
	this->_level = src._level;
	this->_hit_points = src._hit_points;
	this->_max_hit_points = src._max_hit_points;
	this->_energy_points = src._energy_points;
	this->_max_energy_points = src._max_energy_points;
	this->_melee_attack_damage = src._melee_attack_damage;
	this->_ranged_attack_damage = src._ranged_attack_damage;
	this->_armor_damage_reduction = src._armor_damage_reduction;

	std::cout << "Backup done ! Does this mean I can start dancing? Pleeeeeeaaaaase ?" << std::endl;

	return *this;
}

/* Remove then readd because of ex04 use different parent ranged/melee attack */
unsigned int	FragTrap::rangedAttack(std::string const & target) {

	std::cout << "FR4G-TP " << this->_name << " (" << this->_hit_points << "HP) attacks " << target << " at range, causing " << this->_ranged_attack_damage << " points of damage !" << std::endl;

	return this->_ranged_attack_damage;
}

unsigned int	FragTrap::meleeAttack(std::string const & target) {

	std::cout << "FR4G-TP " << this->_name << " (" << this->_hit_points << "HP) attacks " << target << " at melee, causing " << this->_melee_attack_damage << " points of damage !" << std::endl;

	return this->_melee_attack_damage;
}

unsigned int	FragTrap::vaulthunter_dot_exe(std::string const & target) {

	char			rand[4];
	unsigned int	index;
	unsigned int	damage = 0;
	std::string		attacks[] = {
		"Funzerker",
		"Blightbot",
		"Mechromagician",
		"One Shot Wonder",
		"Clap-in-the-Box"
	};

	if (this->_energy_points >= 25) {
		std::ifstream random("/dev/urandom");
		random.read(rand, 4);
		random.close();

		index = damage = (unsigned int)(*rand);
		index %= 5;
		damage %= this->_hit_points / 2;

		std::cout << "FR4G-TP " << this->_name << " attacks " << target << " using " << attacks[index] << ", causing " << damage << " points of damage !" << std::endl;

		this->_energy_points -= 25;
		return damage;
	}

	std::cout << "FR4G-TP " << this->_name << " have not enough energy points to launch vaulhunter.EXE on " << target << " !" << std::endl;
	return damage;
}

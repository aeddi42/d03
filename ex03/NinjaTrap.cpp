/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   NinjaTrap.cpp                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aeddi <aeddi@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/08 15:57:18 by aeddi             #+#    #+#             */
/*   Updated: 2015/04/09 16:34:01 by aeddi            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <iostream>
#include <fstream>
#include "NinjaTrap.hpp"

				NinjaTrap::NinjaTrap(void) : ClapTrap() {

	this->_hit_points = 60;
	this->_max_hit_points = 60;
	this->_energy_points = 120;
	this->_max_energy_points = 120;
	this->_melee_attack_damage = 60;
	this->_ranged_attack_damage = 5;
	this->_armor_damage_reduction = 0;

	std::cout << "Il est le NINJA !" << std::endl;
}

				NinjaTrap::NinjaTrap(std::string const & name) : ClapTrap(name) {

	this->_hit_points = 60;
	this->_max_hit_points = 60;
	this->_energy_points = 120;
	this->_max_energy_points = 120;
	this->_melee_attack_damage = 60;
	this->_ranged_attack_damage = 5;
	this->_armor_damage_reduction = 0;

	std::cout << "Il est le NINJA !" << std::endl;
}


				NinjaTrap::NinjaTrap(NinjaTrap const & src) {

	*this = src;

	std::cout << "Il est le NINJA !" << std::endl;
}


				NinjaTrap::~NinjaTrap(void) {
	std::cout << "Nintchao !" << std::endl;
}


NinjaTrap&		NinjaTrap::operator=(NinjaTrap const & src) {

	this->_name = src._name;
	this->_level = src._level;
	this->_hit_points = src._hit_points;
	this->_max_hit_points = src._max_hit_points;
	this->_energy_points = src._energy_points;
	this->_max_energy_points = src._max_energy_points;
	this->_melee_attack_damage = src._melee_attack_damage;
	this->_ranged_attack_damage = src._ranged_attack_damage;
	this->_armor_damage_reduction = src._armor_damage_reduction;

	std::cout << "Ninja ! I'm a ninja !" << std::endl;

	return *this;
}

/* Remove then readd because of ex04 use different parent ranged/melee attack */
unsigned int	NinjaTrap::rangedAttack(std::string const & target) {

	std::cout << "NINJ4-TP " << this->_name << " (" << this->_hit_points << "HP) attacks " << target << " at range, causing " << this->_ranged_attack_damage << " points of damage !" << std::endl;

	return this->_ranged_attack_damage;
}

unsigned int	NinjaTrap::meleeAttack(std::string const & target) {

	std::cout << "NINJ4-TP " << this->_name << " (" << this->_hit_points << "HP) attacks " << target << " at melee, causing " << this->_melee_attack_damage << " points of damage !" << std::endl;

	return this->_melee_attack_damage;
}

unsigned int	NinjaTrap::ninjaShoebox(ClapTrap const & src, std::string const & name) {

	if (this->_energy_points >= 25) {
		std::cout << "NINJ4-TP " << this->_name << " launch the anti-ClapTrap attack " << name << "!" << std::endl;
		return 80;
	}

	(void)src;
	std::cout << "NINJ4-TP " << this->_name << " have not enough energy points to launch the anti-ClapTrap attack " << name << "!" << std::endl;
	return 0;
}

unsigned int	NinjaTrap::ninjaShoebox(FragTrap const & src, std::string const & name) {

	if (this->_energy_points >= 25) {
		std::cout << "NINJ4-TP " << this->_name << " launch the anti-FragTrap attack " << name << "!" << std::endl;
		return 80;
	}

	(void)src;
	std::cout << "NINJ4-TP " << this->_name << " have not enough energy points to launch the anti-FragTrap attack " << name << "!" << std::endl;
	return 0;
}

unsigned int	NinjaTrap::ninjaShoebox(ScavTrap const & src, std::string const & name) {

	if (this->_energy_points >= 25) {
		std::cout << "NINJ4-TP " << this->_name << " launch the anti-ScavTrap attack " << name << "!" << std::endl;
		return 80;
	}

	(void)src;
	std::cout << "NINJ4-TP " << this->_name << " have not enough energy points to launch the anti-ScavTrap attack " << name << "!" << std::endl;
	return 0;
}

unsigned int	NinjaTrap::ninjaShoebox(NinjaTrap const & src, std::string const & name) {

	if (this->_energy_points >= 25) {
		std::cout << "NINJ4-TP " << this->_name << " launch the anti-NinjaTrap attack " << name << "!" << std::endl;
		return 50;
	}

	(void)src;
	std::cout << "NINJ4-TP " << this->_name << " have not enough energy points to launch the anti-NinjaTrap attack " << name << "!" << std::endl;
	return 0;
}

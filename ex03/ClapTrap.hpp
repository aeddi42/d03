/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ClapTrap.hpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aeddi <aeddi@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/08 17:52:23 by aeddi             #+#    #+#             */
/*   Updated: 2015/04/09 13:42:24 by aeddi            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef CLAPTRAP_HPP
# define CLAPTRAP_HPP

# include <iostream>

class	ClapTrap {

	public:

						ClapTrap(std::string const & name);
						ClapTrap(ClapTrap const & src);
						~ClapTrap(void);
		ClapTrap&		operator=(ClapTrap const & src);

		unsigned int	rangedAttack(std::string const & target);
		unsigned int	meleeAttack(std::string const & target);
		bool			takeDamage(unsigned int amount);
		void			beRepaired(unsigned int amount);

		std::string		getName(void);

	protected:

						ClapTrap(void);

		std::string		_name;
		std::string		_id;
		unsigned int	_level;

		unsigned int	_hit_points;
		unsigned int	_max_hit_points;

		unsigned int	_energy_points;
		unsigned int	_max_energy_points;

		unsigned int	_melee_attack_damage;
		unsigned int	_ranged_attack_damage;
		unsigned int	_armor_damage_reduction;

};

#endif /* !CLAPTRAP_HPP */

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ClapTrap.cpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aeddi <aeddi@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/08 18:00:17 by aeddi             #+#    #+#             */
/*   Updated: 2015/04/09 13:43:40 by aeddi            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <iostream>
#include <fstream>
#include "ClapTrap.hpp"

				ClapTrap::ClapTrap(void) : _name("Remi"), _level(1) {
	std::cout << "I'm the " << this->_name << " CL4P_TP" << std::endl;
}

				ClapTrap::ClapTrap(std::string const & name) : _name(name), _level(1) {
	std::cout << "I'm the " << this->_name << " CL4P_TP" << std::endl;
}

				ClapTrap::ClapTrap(ClapTrap const & src) {
	*this = src;
	std::cout << "I'm the " << this->_name << " CL4P_TP" << std::endl;
}


				ClapTrap::~ClapTrap(void) {
	std::cout << "I'm the " << this->_name << " CL4P_TP and I'm dead" << std::endl;
}


ClapTrap&		ClapTrap::operator=(ClapTrap const & src) {

	this->_name = src._name;
	this->_level = src._level;

	return *this;
}

unsigned int	ClapTrap::rangedAttack(std::string const & target) {

	std::cout << "CL4P_TP " << this->_name << " (" << this->_hit_points << "HP) attacks " << target << " at range, causing " << this->_ranged_attack_damage << " points of damage !" << std::endl;

	return this->_ranged_attack_damage;
}

unsigned int	ClapTrap::meleeAttack(std::string const & target) {

	std::cout << "CL4P_TP " << this->_name << " (" << this->_hit_points << "HP) attacks " << target << " at melee, causing " << this->_melee_attack_damage << " points of damage !" << std::endl;

	return this->_melee_attack_damage;
}

bool			ClapTrap::takeDamage(unsigned int amount) {

	unsigned int	damage;

	damage = (this->_armor_damage_reduction > amount) ? 0 : amount - this->_armor_damage_reduction;
	std::cout << "CL4P-TP " << this->_name << " (" << this->_hit_points << "HP) takes damage (armor save " << this->_armor_damage_reduction << " points) and loses " << damage << " HP ! ";

	this->_hit_points = (this->_hit_points >= damage) ? this->_hit_points - damage : 0;
	std::cout << "Rest " << this->_hit_points << " HP." << std::endl;

	if (this->_hit_points)
		return true;
	else
		return false;
}

void			ClapTrap::beRepaired(unsigned int amount) {

	unsigned int	reparation;

	reparation = (this->_hit_points + amount > this->_max_hit_points) ? this->_max_hit_points - this->_hit_points : amount;
	std::cout << "CL4P-TP " << this->_name << " (" << this->_hit_points << "HP) be repaired and recover " << reparation << " HP ! ";

	this->_hit_points += reparation;
	std::cout << "Rest " << this->_hit_points << " HP." << std::endl;

	return;
}

std::string		ClapTrap::getName(void) {

	return this->_name;
}

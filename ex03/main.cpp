/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aeddi <aeddi@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/08 12:23:31 by aeddi             #+#    #+#             */
/*   Updated: 2015/04/09 14:16:30 by aeddi            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <iostream>
#include <fstream>
#include "FragTrap.hpp"
#include "ScavTrap.hpp"
#include "NinjaTrap.hpp"

unsigned int	ft_random(void) {

	char			rand[4];

	std::ifstream random("/dev/urandom");
	random.read(rand, 4);
	random.close();

	return (unsigned int)(*rand);
}

void			choose_players(unsigned int & stricker, unsigned int & defender, unsigned int & repaired, ClapTrap *players[]) {

	stricker = defender = 0;
	while (stricker == defender || players[stricker] == NULL || players[defender] == NULL) {
		stricker = ft_random() % 6;
		defender = ft_random() % 6;
	}
	repaired = ft_random() % 14;

	return;
}

int				main(void) {

	unsigned int	round = 1;
	unsigned int	alive = 6;
	std::string		names[] = {
		"Axel",
		"Gody",
		"Francky",
		"Sacha",
		"Mathieu",
		"Antoine"
	};
	ClapTrap		*players[] = {
		new FragTrap(names[0]),
		new FragTrap(names[1]),
		new ScavTrap(names[2]),
		new ScavTrap(names[3]),
		new NinjaTrap(names[4]),
		new NinjaTrap(names[5])
	};
	unsigned int	attack;
	unsigned int	stricker;
	unsigned int	defender;
	unsigned int	repaired;
	unsigned int	damage;
	unsigned int	winner = 0;

	std::cout << std::endl;
	while (alive > 1) {
		std::cout << "Round " << round++ << ": FIGHT !" << std::endl;
		choose_players(stricker, defender, repaired, players);
		attack = ft_random() % 3;

		if (attack == 0) {
			if (stricker < 2)
				damage = ((FragTrap *)players[stricker])->rangedAttack((players[defender])->getName());
			else if (stricker > 3)
				damage = ((NinjaTrap *)players[stricker])->rangedAttack((players[defender])->getName());
			else
				damage = ((ScavTrap *)players[stricker])->rangedAttack((players[defender])->getName());
		}
		else if (attack == 1) {
			if (stricker < 2)
				damage = ((FragTrap *)players[stricker])->rangedAttack((players[defender])->getName());
			else if (stricker > 3)
				damage = ((NinjaTrap *)players[stricker])->rangedAttack((players[defender])->getName());
			else
				damage = ((ScavTrap *)players[stricker])->rangedAttack((players[defender])->getName());
		}
		else if (attack == 2)
		{
			if (stricker < 2)
				damage = ((FragTrap *)players[stricker])->vaulthunter_dot_exe((players[defender])->getName());
			else if (stricker < 4)
				damage = ((ScavTrap *)players[stricker])->challengeNewcomer((players[defender])->getName());
			else {
				if (defender < 2)
					damage = ((NinjaTrap *)players[stricker])->ninjaShoebox(((FragTrap &)players[defender]), players[defender]->getName());
				else if (defender < 4)
					damage = ((NinjaTrap *)players[stricker])->ninjaShoebox(((ScavTrap &)players[defender]), players[defender]->getName());
				else
					damage = ((NinjaTrap *)players[stricker])->ninjaShoebox(((NinjaTrap &)players[defender]), players[defender]->getName());
			}
		}

		if (!players[defender]->takeDamage(damage)) {
			delete players[defender];
			players[defender] = NULL;
			alive--;
		}
		if (repaired < 6 && players[repaired] != NULL)
			players[repaired]->beRepaired(ft_random() % 42);
		std::cout << std::endl;
	}

	while (winner < 6 && players[winner] == NULL)
		winner++;
	std::cout << "And the winner is: " << names[winner] << " !" << std::endl;

	return 0;
}

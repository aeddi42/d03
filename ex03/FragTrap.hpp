/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   FragTrap.hpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aeddi <aeddi@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/08 10:01:34 by aeddi             #+#    #+#             */
/*   Updated: 2015/04/09 16:28:10 by aeddi            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FRAGTRAP_HPP
# define FRAGTRAP_HPP

# include <iostream>
# include "ClapTrap.hpp"

class	FragTrap : public ClapTrap {

	public:

						FragTrap(std::string const & name);
						FragTrap(FragTrap const & src);
						~FragTrap(void);
		FragTrap&		operator=(FragTrap const & src);

		unsigned int	vaulthunter_dot_exe(std::string const & target);
		unsigned int	rangedAttack(std::string const & target);
		unsigned int	meleeAttack(std::string const & target);

	private:

						FragTrap(void);

};

#endif /* !FRAGTRAP_HPP */

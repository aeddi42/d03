/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ScavTrap.hpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aeddi <aeddi@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/08 15:57:35 by aeddi             #+#    #+#             */
/*   Updated: 2015/04/09 12:20:41 by aeddi            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef SCAVTRAP_HPP
# define SCAVTRAP_HPP

# include <iostream>

class	ScavTrap {

	public:

						ScavTrap(std::string const & name);
						ScavTrap(ScavTrap const & src);
						~ScavTrap(void);
		ScavTrap&		operator=(ScavTrap const & src);

		unsigned int	rangedAttack(std::string const & target);
		unsigned int	meleeAttack(std::string const & target);
		int				challengeNewcomer(std::string const & target);
		bool			takeDamage(unsigned int amount);
		void			beRepaired(unsigned int amount);

		std::string		getName(void);

	private:

						ScavTrap(void);

		std::string		_name;
		unsigned int	_level;

		unsigned int	_hit_points;
		unsigned int	_max_hit_points;

		unsigned int	_energy_points;
		unsigned int	_max_energy_points;

		unsigned int	_melee_attack_damage;
		unsigned int	_ranged_attack_damage;
		unsigned int	_armor_damage_reduction;

};

#endif /* !SCAVTRAP_HPP */

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   FragTrap.hpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aeddi <aeddi@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/08 10:01:34 by aeddi             #+#    #+#             */
/*   Updated: 2015/01/08 14:55:00 by aeddi            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FRAGTRAP_HPP
# define FRAGTRAP_HPP

# include <iostream>

class	FragTrap {

	public:

						FragTrap(std::string const & name);
						FragTrap(FragTrap const & src);
						~FragTrap(void);
		FragTrap&		operator=(FragTrap const & src);

		unsigned int	rangedAttack(std::string const & target);
		unsigned int	meleeAttack(std::string const & target);
		unsigned int	vaulthunter_dot_exe(std::string const & target);
		bool			takeDamage(unsigned int amount);
		void			beRepaired(unsigned int amount);

		std::string		getName(void);

	private:

						FragTrap(void);

		std::string		_name;
		unsigned int	_level;

		unsigned int	_hit_points;
		unsigned int	_max_hit_points;

		unsigned int	_energy_points;
		unsigned int	_max_energy_points;

		unsigned int	_melee_attack_damage;
		unsigned int	_ranged_attack_damage;
		unsigned int	_armor_damage_reduction;

};

#endif /* !FRAGTRAP_HPP */

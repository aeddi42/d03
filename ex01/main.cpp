/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aeddi <aeddi@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/08 12:23:31 by aeddi             #+#    #+#             */
/*   Updated: 2015/04/09 12:26:19 by aeddi            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <iostream>
#include <fstream>
#include "FragTrap.hpp"
#include "ScavTrap.hpp"

unsigned int	ft_random(void) {

	char			rand[4];

	std::ifstream random("/dev/urandom");
	random.read(rand, 4);
	random.close();

	return (unsigned int)(*rand);
}

void			choose_players(unsigned int & stricker, unsigned int & defender, unsigned int & repaired, FragTrap *players[]) {

	stricker = defender = 0;
	while (stricker == defender || players[stricker] == NULL || players[defender] == NULL) {
		stricker = ft_random() % 4;
		defender = ft_random() % 4;
	}
	repaired = ft_random() % 10;

	return;
}

int				main(void) {

	unsigned int	round = 1;
	unsigned int	alive = 4;
	std::string		names[] = {
		"Axel",
		"Gody",
		"Francky",
		"Sacha"
	};
	void		*players[] = {
		new FragTrap(names[0]),
		new FragTrap(names[1]),
		new ScavTrap(names[2]),
		new ScavTrap(names[3])
	};
	unsigned int	attack;
	unsigned int	stricker;
	unsigned int	defender;
	unsigned int	repaired;
	unsigned int	damage;
	unsigned int	winner = 0;

	std::cout << std::endl;

	while (alive > 1) {
		std::cout << "Round " << round++ << ": FIGHT !" << std::endl;
		choose_players(stricker, defender, repaired, ((FragTrap **)players));
		attack = ft_random() % 3;

		if (attack == 0) {
			if (stricker < 2 && defender < 2)
				damage = ((FragTrap *)players[stricker])->rangedAttack(((FragTrap *)players[defender])->getName());
			else if (stricker < 2 && defender > 1)
				damage = ((FragTrap *)players[stricker])->rangedAttack(((ScavTrap *)players[defender])->getName());
			else if (stricker > 1 && defender > 1)
				damage = ((ScavTrap *)players[stricker])->rangedAttack(((ScavTrap *)players[defender])->getName());
			else
				damage = ((ScavTrap *)players[stricker])->rangedAttack(((FragTrap *)players[defender])->getName());
		}
		else if (attack == 1) {
			if (stricker < 2 && defender < 2)
				damage = ((FragTrap *)players[stricker])->meleeAttack(((FragTrap *)players[defender])->getName());
			else if (stricker < 2 && defender > 1)
				damage = ((FragTrap *)players[stricker])->meleeAttack(((ScavTrap *)players[defender])->getName());
			else if (stricker > 1 && defender > 1)
				damage = ((ScavTrap *)players[stricker])->meleeAttack(((ScavTrap *)players[defender])->getName());
			else
				damage = ((ScavTrap *)players[stricker])->meleeAttack(((FragTrap *)players[defender])->getName());
		}
		else if (attack == 2) {
			if (stricker < 2 && defender < 2)
				damage = ((FragTrap *)players[stricker])->vaulthunter_dot_exe(((FragTrap *)players[defender])->getName());
			else if (stricker < 2 && defender > 1)
				damage = ((FragTrap *)players[stricker])->vaulthunter_dot_exe(((ScavTrap *)players[defender])->getName());
			else if (stricker > 1 && defender > 1)
				damage = ((ScavTrap *)players[stricker])->challengeNewcomer(((ScavTrap *)players[defender])->getName());
			else
				damage = ((ScavTrap *)players[stricker])->challengeNewcomer(((FragTrap *)players[defender])->getName());
		}

		if (defender < 2 && !((FragTrap *)players[defender])->takeDamage(damage)) {
			delete (FragTrap *)players[defender];
			players[defender] = NULL;
			alive--;
		}
		else if (defender > 1 && !((ScavTrap *)players[defender])->takeDamage(damage)) {
			delete (ScavTrap *)players[defender];
			players[defender] = NULL;
			alive--;
		}
		if (repaired < 4 && players[repaired] != NULL)
		{
			if (repaired < 2)
				((FragTrap *)players[repaired])->beRepaired(ft_random() % 42);
			else
				((ScavTrap *)players[repaired])->beRepaired(ft_random() % 42);
		}
		std::cout << std::endl;
	}

	while (winner < 4 && players[winner] == NULL)
		winner++;
	std::cout << "And the winner is: " << names[winner] << " !" << std::endl;

	return 0;
}

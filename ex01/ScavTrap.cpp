/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ScavTrap.cpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aeddi <aeddi@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/08 15:57:18 by aeddi             #+#    #+#             */
/*   Updated: 2015/04/09 12:20:44 by aeddi            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <iostream>
#include <fstream>
#include "ScavTrap.hpp"

				ScavTrap::ScavTrap(void) :
	_name("Remi"),
	_level(1),
	_hit_points(100),
	_max_hit_points(100),
	_energy_points(50),
	_max_energy_points(50),
	_melee_attack_damage(20),
	_ranged_attack_damage(15),
	_armor_damage_reduction(3) {

	std::cout << "Hello World !" << std::endl;
}

				ScavTrap::ScavTrap(std::string const & name) :
	_name(name),
	_level(1),
	_hit_points(100),
	_max_hit_points(100),
	_energy_points(50),
	_max_energy_points(50),
	_melee_attack_damage(20),
	_ranged_attack_damage(15),
	_armor_damage_reduction(3) {

	std::cout << "Hello World !" << std::endl;
}


				ScavTrap::ScavTrap(ScavTrap const & src) {

	*this = src;

	std::cout << "Hello World !" << std::endl;
}


				ScavTrap::~ScavTrap(void) {
	std::cout << "I'll stop talking when I'm dead !" << std::endl;
}


ScavTrap&		ScavTrap::operator=(ScavTrap const & src) {

	this->_name = src._name;
	this->_level = src._level;
	this->_hit_points = src._hit_points;
	this->_max_hit_points = src._max_hit_points;
	this->_energy_points = src._energy_points;
	this->_max_energy_points = src._max_energy_points;
	this->_melee_attack_damage = src._melee_attack_damage;
	this->_ranged_attack_damage = src._ranged_attack_damage;
	this->_armor_damage_reduction = src._armor_damage_reduction;

	std::cout << "Hello World ! I'm a clone !" << std::endl;

	return *this;
}

unsigned int	ScavTrap::rangedAttack(std::string const & target) {

	std::cout << "SC4V-TP " << this->_name << " (" << this->_hit_points << "HP) attacks " << target << " at range, causing " << this->_ranged_attack_damage << " points of damage !" << std::endl;

	return this->_ranged_attack_damage;
}

unsigned int	ScavTrap::meleeAttack(std::string const & target) {

	std::cout << "SC4V-TP " << this->_name << " (" << this->_hit_points << "HP) attacks " << target << " at melee, causing " << this->_melee_attack_damage << " points of damage !" << std::endl;

	return this->_melee_attack_damage;
}

bool			ScavTrap::takeDamage(unsigned int amount) {

	unsigned int	damage;

	damage = (this->_armor_damage_reduction > amount) ? 0 : amount - this->_armor_damage_reduction;
	std::cout << "SC4V-TP " << this->_name << " (" << this->_hit_points << "HP) takes damage (armor save " << this->_armor_damage_reduction << " points) and loses " << damage << " HP ! ";

	this->_hit_points = (this->_hit_points >= damage) ? this->_hit_points - damage : 0;
	std::cout << "Rest " << this->_hit_points << " HP." << std::endl;

	if (this->_hit_points)
		return true;
	else
		return false;
}

void			ScavTrap::beRepaired(unsigned int amount) {

	unsigned int	reparation;

	reparation = (this->_hit_points + amount > this->_max_hit_points) ? this->_max_hit_points - this->_hit_points : amount;
	std::cout << "SC4V-TP " << this->_name << " (" << this->_hit_points << "HP) be repaired and recover " << reparation << " HP ! ";

	this->_hit_points += reparation;
	std::cout << "Rest " << this->_hit_points << " HP." << std::endl;

	return;
}

int				ScavTrap::challengeNewcomer(std::string const & target) {

	char			rand[4];
	unsigned int	index;
	std::string		challenges[] = {
		"\"DRINK ! DRINK ! DRINK !\"",
		"\"Touch your nipple with your own tongue !",
		"\"Give me one million dollars if you're a robot !\"",
		"\"Loser of the sack-race pay me a beer !\"",
		"\"Do a barel roll !\""
	};

	if (this->_energy_points >= 25) {
		std::ifstream random("/dev/urandom");
		random.read(rand, 4);
		random.close();

		index = (unsigned int)(*rand);
		index %= 5;

		std::cout << "SC4V-TP " << this->_name << " challenges " << target << " with " << challenges[index] << std::endl;

		this->_energy_points -= 25;
	}
	else
		std::cout << "SC4V-TP " << this->_name << " have not enough energy points to launch challenge to " << target << " !" << std::endl;
	return 0;
}

std::string		ScavTrap::getName(void) {

	return this->_name;
}

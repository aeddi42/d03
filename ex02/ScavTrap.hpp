/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ScavTrap.hpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aeddi <aeddi@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/08 15:57:35 by aeddi             #+#    #+#             */
/*   Updated: 2015/04/09 16:30:40 by aeddi            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef SCAVTRAP_HPP
# define SCAVTRAP_HPP

# include <iostream>
# include "ClapTrap.hpp"

class	ScavTrap : public ClapTrap {

	public:

						ScavTrap(std::string const & name);
						ScavTrap(ScavTrap const & src);
						~ScavTrap(void);
		ScavTrap&		operator=(ScavTrap const & src);

		int				challengeNewcomer(std::string const & target);
		unsigned int	rangedAttack(std::string const & target);
		unsigned int	meleeAttack(std::string const & target);

	private:

						ScavTrap(void);

};

#endif /* !SCAVTRAP_HPP */

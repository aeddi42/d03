/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   FragTrap.cpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aeddi <aeddi@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/08 10:01:34 by aeddi             #+#    #+#             */
/*   Updated: 2015/04/09 11:51:18 by aeddi            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <iostream>
#include <fstream>
#include "FragTrap.hpp"

				FragTrap::FragTrap(void) :
	_name("Remi"),
	_level(1),
	_hit_points(100),
	_max_hit_points(100),
	_energy_points(100),
	_max_energy_points(100),
	_melee_attack_damage(30),
	_ranged_attack_damage(20),
	_armor_damage_reduction(5) {

	std::cout << "Directive one: Protect humanity! Directive two: Dance!" << std::endl;
	std::cout << "Commencing directive two! Uhntssuhntssuhntss--" << std::endl;
}

				FragTrap::FragTrap(std::string const & name) : 
	_name(name),
	_level(1),
	_hit_points(100),
	_max_hit_points(100),
	_energy_points(100),
	_max_energy_points(100),
	_melee_attack_damage(30),
	_ranged_attack_damage(20),
	_armor_damage_reduction(5) {

	std::cout << "Directive one: Protect humanity! Directive two: Dance!" << std::endl;
	std::cout << "Commencing directive two! Uhntssuhntssuhntss--" << std::endl;
}


				FragTrap::FragTrap(FragTrap const & src) {

	*this = src;

	std::cout << "Directive one: Protect humanity! Directive two: Dance!" << std::endl;
	std::cout << "Commencing directive two! Uhntssuhntssuhntss--" << std::endl;
}


				FragTrap::~FragTrap(void) {
	std::cout << "I'M DEAD I'M DEAD OHMYGOD I'M DEAD!" << std::endl;
}


FragTrap&		FragTrap::operator=(FragTrap const & src) {

	this->_name = src._name;
	this->_level = src._level;
	this->_hit_points = src._hit_points;
	this->_max_hit_points = src._max_hit_points;
	this->_energy_points = src._energy_points;
	this->_max_energy_points = src._max_energy_points;
	this->_melee_attack_damage = src._melee_attack_damage;
	this->_ranged_attack_damage = src._ranged_attack_damage;
	this->_armor_damage_reduction = src._armor_damage_reduction;

	std::cout << "Backup done ! Does this mean I can start dancing? Pleeeeeeaaaaase ?" << std::endl;

	return *this;
}

unsigned int	FragTrap::rangedAttack(std::string const & target) {

	std::cout << "FR4G-TP " << this->_name << " (" << this->_hit_points << "HP) attacks " << target << " at range, causing " << this->_ranged_attack_damage << " points of damage !" << std::endl;

	return this->_ranged_attack_damage;
}

unsigned int	FragTrap::meleeAttack(std::string const & target) {

	std::cout << "FR4G-TP " << this->_name << " (" << this->_hit_points << "HP) attacks " << target << " at melee, causing " << this->_melee_attack_damage << " points of damage !" << std::endl;

	return this->_melee_attack_damage;
}

bool			FragTrap::takeDamage(unsigned int amount) {

	unsigned int	damage;

	damage = (this->_armor_damage_reduction > amount) ? 0 : amount - this->_armor_damage_reduction;
	std::cout << "FR4G-TP " << this->_name << " (" << this->_hit_points << "HP) takes damage (armor save " << this->_armor_damage_reduction << " points) and loses " << damage << " HP ! ";

	this->_hit_points = (this->_hit_points >= damage) ? this->_hit_points - damage : 0;
	std::cout << "Rest " << this->_hit_points << " HP." << std::endl;

	if (this->_hit_points)
		return true;
	else
		return false;
}

void			FragTrap::beRepaired(unsigned int amount) {

	unsigned int	reparation;

	reparation = (this->_hit_points + amount > this->_max_hit_points) ? this->_max_hit_points - this->_hit_points : amount;
	std::cout << "FR4G-TP " << this->_name << " (" << this->_hit_points << "HP) be repaired and recover " << reparation << " HP ! ";

	this->_hit_points += reparation;
	std::cout << "Rest " << this->_hit_points << " HP." << std::endl;

	return;
}

unsigned int	FragTrap::vaulthunter_dot_exe(std::string const & target) {

	char			rand[4];
	unsigned int	index;
	unsigned int	damage = 0;
	std::string		attacks[] = {
		"Funzerker",
		"Blightbot",
		"Mechromagician",
		"One Shot Wonder",
		"Clap-in-the-Box"
	};

	if (this->_energy_points >= 25) {
		std::ifstream random("/dev/urandom");
		random.read(rand, 4);
		random.close();

		index = damage = (unsigned int)(*rand);
		index %= 5;
		damage %= this->_hit_points / 2;

		std::cout << "FR4G-TP " << this->_name << " attacks " << target << " using " << attacks[index] << ", causing " << damage << " points of damage !" << std::endl;

		this->_energy_points -= 25;
		return damage;
	}

	std::cout << "FR4G-TP " << this->_name << " have not enough energy points to launch vaulhunter.EXE on " << target << " !" << std::endl;
	return damage;
}

std::string		FragTrap::getName(void) {

	return this->_name;
}

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aeddi <aeddi@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/08 12:23:31 by aeddi             #+#    #+#             */
/*   Updated: 2015/04/09 11:42:03 by aeddi            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <iostream>
#include <fstream>
#include "FragTrap.hpp"

unsigned int	ft_random(void) {

	char			rand[4];

	std::ifstream random("/dev/urandom");
	random.read(rand, 4);
	random.close();

	return (unsigned int)(*rand);
}

void			choose_players(unsigned int & stricker, unsigned int & defender, unsigned int & repaired, FragTrap *players[4]) {

	stricker = defender = 0;
	while (stricker == defender || players[stricker] == NULL || players[defender] == NULL) {
		stricker = ft_random() % 4;
		defender = ft_random() % 4;
	}
	repaired = ft_random() % 10;

	return;
}

int				main(void) {

	unsigned int	round = 1;
	unsigned int	alive = 4;
	std::string		names[] = {
		"Axel",
		"Gody",
		"Francky",
		"Sacha"
	};
	FragTrap		*players[] = {
		new FragTrap(names[0]),
		new FragTrap(names[1]),
		new FragTrap(names[2]),
		new FragTrap(names[3])
	};
	unsigned int	attack;
	unsigned int	stricker;
	unsigned int	defender;
	unsigned int	repaired;
	unsigned int	damage;
	unsigned int	winner = 0;

	std::cout << std::endl;

	while (alive > 1) {
		std::cout << "Round " << round++ << ": FIGHT !" << std::endl;
		choose_players(stricker, defender, repaired, players);
		attack = ft_random() % 3;

		if (attack == 0)
			damage = players[stricker]->rangedAttack(players[defender]->getName());
		else if (attack == 1)
			damage = players[stricker]->meleeAttack(players[defender]->getName());
		else
			damage = players[stricker]->vaulthunter_dot_exe(players[defender]->getName());

		if (!players[defender]->takeDamage(damage)) {
			delete players[defender];
			players[defender] = NULL;
			alive--;
		}
		if (repaired < 4 && players[repaired] != NULL)
			players[repaired]->beRepaired(ft_random() % 42);
		std::cout << std::endl;
	}

	while (winner < 4 && players[winner] == NULL)
		winner++;
	std::cout << "And the winner is: " << names[winner] << " !" << std::endl;

	return 0;
}

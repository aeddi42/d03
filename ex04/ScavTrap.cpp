/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ScavTrap.cpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aeddi <aeddi@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/08 15:57:18 by aeddi             #+#    #+#             */
/*   Updated: 2015/04/09 16:30:42 by aeddi            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <iostream>
#include <fstream>
#include "ScavTrap.hpp"

				ScavTrap::ScavTrap(void) : ClapTrap() {

	this->_hit_points = 100;
	this->_max_hit_points = 100;
	this->_energy_points = 50;
	this->_max_energy_points = 50;
	this->_melee_attack_damage = 20;
	this->_ranged_attack_damage = 15;
	this->_armor_damage_reduction = 3;

	std::cout << "Hello World !" << std::endl;
}

				ScavTrap::ScavTrap(std::string const & name) : ClapTrap(name) {

	this->_hit_points = 100;
	this->_max_hit_points = 100;
	this->_energy_points = 50;
	this->_max_energy_points = 50;
	this->_melee_attack_damage = 20;
	this->_ranged_attack_damage = 15;
	this->_armor_damage_reduction = 3;

	std::cout << "Hello World !" << std::endl;
}


				ScavTrap::ScavTrap(ScavTrap const & src) {

	*this = src;

	std::cout << "Hello World !" << std::endl;
}


				ScavTrap::~ScavTrap(void) {
	std::cout << "I'll stop talking when I'm dead !" << std::endl;
}


ScavTrap&		ScavTrap::operator=(ScavTrap const & src) {

	this->_name = src._name;
	this->_level = src._level;
	this->_hit_points = src._hit_points;
	this->_max_hit_points = src._max_hit_points;
	this->_energy_points = src._energy_points;
	this->_max_energy_points = src._max_energy_points;
	this->_melee_attack_damage = src._melee_attack_damage;
	this->_ranged_attack_damage = src._ranged_attack_damage;
	this->_armor_damage_reduction = src._armor_damage_reduction;

	std::cout << "Hello World ! I'm a clone !" << std::endl;

	return *this;
}

/* Remove then readd because of ex04 use different parent ranged/melee attack */
unsigned int	ScavTrap::rangedAttack(std::string const & target) {

	std::cout << "SC4V-TP " << this->_name << " (" << this->_hit_points << "HP) attacks " << target << " at range, causing " << this->_ranged_attack_damage << " points of damage !" << std::endl;

	return this->_ranged_attack_damage;
}

unsigned int	ScavTrap::meleeAttack(std::string const & target) {

	std::cout << "SC4V-TP " << this->_name << " (" << this->_hit_points << "HP) attacks " << target << " at melee, causing " << this->_melee_attack_damage << " points of damage !" << std::endl;

	return this->_melee_attack_damage;
}

int				ScavTrap::challengeNewcomer(std::string const & target) {

	char			rand[4];
	unsigned int	index;
	std::string		challenges[] = {
		"\"DRINK ! DRINK ! DRINK !\"",
		"\"Touch your nipple with your own tongue !",
		"\"Give me one million dollars if you're a robot !\"",
		"\"Loser of the sack-race pay me a beer !\"",
		"\"Do a barel roll !\""
	};

	if (this->_energy_points >= 25) {
		std::ifstream random("/dev/urandom");
		random.read(rand, 4);
		random.close();

		index = (unsigned int)(*rand);
		index %= 5;

		std::cout << "SC4V-TP " << this->_name << " challenges " << target << " with " << challenges[index] << std::endl;

		this->_energy_points -= 25;
	}
	else
		std::cout << "SC4V-TP " << this->_name << " have not enough energy points to launch challenge to " << target << " !" << std::endl;
	return 0;
}

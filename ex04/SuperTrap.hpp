/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   SuperTrap.hpp                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aeddi <aeddi@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/08 10:01:34 by aeddi             #+#    #+#             */
/*   Updated: 2015/04/09 16:40:36 by aeddi            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef SUPERTRAP_HPP
# define SUPERTRAP_HPP

# include <iostream>
# include "FragTrap.hpp"
# include "NinjaTrap.hpp"

class	SuperTrap : public FragTrap, public NinjaTrap {

	public:

						SuperTrap(std::string const & name);
						SuperTrap(SuperTrap const & src);
						~SuperTrap(void);
		SuperTrap&		operator=(SuperTrap const & src);

		using			FragTrap::rangedAttack;
		using			NinjaTrap::meleeAttack;

	protected:

						SuperTrap(void);

};

#endif /* !SUPERTRAP_HPP */

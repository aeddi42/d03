/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   SuperTrap.cpp                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aeddi <aeddi@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/08 10:01:34 by aeddi             #+#    #+#             */
/*   Updated: 2015/04/09 16:40:25 by aeddi            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <iostream>
#include <fstream>
#include "SuperTrap.hpp"


				SuperTrap::SuperTrap(void) : ClapTrap() {

		this->_hit_points = FragTrap::_hit_points;
		this->_max_hit_points = FragTrap::_max_hit_points;
		this->_energy_points = NinjaTrap::_energy_points;
		this->_max_energy_points = NinjaTrap::_max_energy_points;
		this->_melee_attack_damage = NinjaTrap::_melee_attack_damage;
		this->_ranged_attack_damage = FragTrap::_ranged_attack_damage;
		this->_armor_damage_reduction = FragTrap::_armor_damage_reduction;

		std::cout << "It's a plane ? It's a bird ? No it's a SUPERTRAP !" << std::endl;
}

				SuperTrap::SuperTrap(std::string const & name) : ClapTrap(name) {

		this->_hit_points = FragTrap::_hit_points;
		this->_max_hit_points = FragTrap::_max_hit_points;
		this->_energy_points = NinjaTrap::_energy_points;
		this->_max_energy_points = NinjaTrap::_max_energy_points;
		this->_melee_attack_damage = NinjaTrap::_melee_attack_damage;
		this->_ranged_attack_damage = FragTrap::_ranged_attack_damage;
		this->_armor_damage_reduction = FragTrap::_armor_damage_reduction;

		std::cout << "It's a plane ? It's a bird ? No it's a SUPERTRAP !" << std::endl;
}

				SuperTrap::SuperTrap(SuperTrap const & src) {

		*this = src;

		std::cout << "It's a plane ? It's a bird ? No it's a SUPERTRAP !" << std::endl;
}

				SuperTrap::~SuperTrap(void) {
		std::cout << "NOOOO ! Kryptonite got me !" << std::endl;
}

SuperTrap&		SuperTrap::operator=(SuperTrap const & src) {

	this->_name = src._name;
	this->_level = src._level;
	this->_hit_points = src._hit_points;
	this->_max_hit_points = src._max_hit_points;
	this->_energy_points = src._energy_points;
	this->_max_energy_points = src._max_energy_points;
	this->_melee_attack_damage = src._melee_attack_damage;
	this->_ranged_attack_damage = src._ranged_attack_damage;
	this->_armor_damage_reduction = src._armor_damage_reduction;

	std::cout << "Supertrap twice itself !" << std::endl;

	return *this;
}

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aeddi <aeddi@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/08 12:23:31 by aeddi             #+#    #+#             */
/*   Updated: 2015/04/09 16:49:22 by aeddi            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <iostream>
#include "FragTrap.hpp"
#include "ScavTrap.hpp"
#include "NinjaTrap.hpp"
#include "SuperTrap.hpp"

int				main(void) {

	ClapTrap	gody("Gody");
	FragTrap	axel("Axel");
	ScavTrap	sacha("Sacha");
	NinjaTrap	mathieu("Mathieu");
	SuperTrap	antoine("Antoine");

	std::cout << "==============================================" << std::endl << std::endl;

	antoine.ninjaShoebox(gody, "Gody");
	antoine.ninjaShoebox(axel, "Axel");
	antoine.ninjaShoebox(sacha, "Sacha");
	antoine.ninjaShoebox(mathieu, "Mathieu");
	antoine.vaulthunter_dot_exe("Gody");
	antoine.takeDamage(50);
	antoine.beRepaired(70);
	antoine.rangedAttack("Gody");
	antoine.meleeAttack("Gody");

	std::cout << "==============================================" << std::endl << std::endl;
	return 0;
}

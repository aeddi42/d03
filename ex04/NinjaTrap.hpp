/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   NinjaTrap.hpp                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aeddi <aeddi@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/08 15:57:35 by aeddi             #+#    #+#             */
/*   Updated: 2015/04/09 16:44:21 by aeddi            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef NINJATRAP_HPP
# define NINJATRAP_HPP

# include <iostream>
# include "FragTrap.hpp"
# include "ScavTrap.hpp"
# include "ClapTrap.hpp"

class	NinjaTrap : virtual public ClapTrap {

	public:

						NinjaTrap(std::string const & name);
						NinjaTrap(NinjaTrap const & src);
						~NinjaTrap(void);
		NinjaTrap&		operator=(NinjaTrap const & src);

		unsigned int	ninjaShoebox(ClapTrap const & src, std::string const & name);
		unsigned int	ninjaShoebox(FragTrap const & src, std::string const & name);
		unsigned int	ninjaShoebox(ScavTrap const & src, std::string const & name);
		unsigned int	ninjaShoebox(NinjaTrap const & src, std::string const & name);
		unsigned int	rangedAttack(std::string const & target);
		unsigned int	meleeAttack(std::string const & target);

	protected:

						NinjaTrap(void);

};

#endif /* !NINJATRAP_HPP */
